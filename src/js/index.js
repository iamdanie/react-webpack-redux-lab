import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./storage/index";
import { App } from "./components/App";
import ErrorBoundary from './components/error/ErrorBoundary';
import 'semantic-ui-css/semantic.min.css';
import 'react-table/react-table.css';
import '../../assets/styles/styles.css'

render(
  	<Provider store={store}>
    	<App />
  	</Provider>,
  document.getElementById("app")
);
