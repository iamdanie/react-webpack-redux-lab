import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoginForm from '../../components/auth/LoginForm';
import { authActions } from '../../storage/auth/auth';

class LogInPage extends Component {
	constructor(props) {
		super(props);

		this.props.dispatch(authActions.logout());
		this.onLogIn = this.onLogIn.bind(this);
	}

	onLogIn(props) {		
		this.setState({ submitted: true });

		const { dispatch } = this.props;
		dispatch(authActions.login(props.email, props.password));
	}

	render() {
		return (
			<LoginForm onLogin={this.onLogIn} />
		)
	};
}

function mapStateToProps(state) {
	const { loggingIn } = state.authController;
	return {
			loggingIn
	};
};

const connectedLoginPage = connect(mapStateToProps)(LogInPage);
export { connectedLoginPage as LogInPage };
