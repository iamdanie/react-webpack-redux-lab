import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import rootReducer from '../controllers/index';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();
const loggerMiddleware = createLogger();

const store = createStore(
	rootReducer,
	{},
	applyMiddleware(
		thunk,
		loggerMiddleware
	)
);

export default store;
