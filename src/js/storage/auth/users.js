import userService from '../../services/authService';
import { GETALL_REQUEST, GETALL_SUCCESS, GETALL_FAILURE } from './actions'

export const userActions = { getAll };

function request() { 
	return { 
		type: GETALL_REQUEST 
	}
};

function success(users) { 
	return { 
		type: GETALL_SUCCESS, users 
	} 
};

function failure(error) { 
	return { 
		type: GETALL_FAILURE, 
		error 
	} 
};

function getAll() {
	return dispatch => {
		dispatch(request());

		userService.getAll()
			.then(
				users => dispatch(success(users)),
				error => dispatch(failure(error))
			);
	};
}