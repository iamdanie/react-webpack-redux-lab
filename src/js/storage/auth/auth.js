import { userService } from '../../services/authService';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from './actions';
import { history } from '../index';

export const authActions = {
	login,
	logout
};

function request(user) { 
	return { 
		type: LOGIN_REQUEST, 
		user 
	} 
};

function success(user) { 
	return { 
		type: LOGIN_SUCCESS, 
		user 
	} 
};

function failure(error) { 
	return { 
		type: LOGIN_FAILURE, 
		error
	}
};

function login(email, password) {
	return dispatch => {
		dispatch(request({ email }));

		userService.login(email, password)
		.then(function(user){
			dispatch(success(user));
			history.push('/');
		});
	};
};

function logout() {
	userService.logout();
	return { 
		type: LOGOUT 
	};
};