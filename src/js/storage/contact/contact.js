import { ADD_CONTACT } from "./actions";
import { FETCH_CONTACTS } from "./actions";
import {list, register} from "../../services/contact"

export const addContact = contact => {
    return(dispatch) => {
        return register(contact)
            .then(response => dispatch({type: ADD_CONTACT, payload: response.data}))
    }
}

export const fetchAllContacts = () => {
    return (dispatch) => {
        return list()
            .then(response => {dispatch({ type: FETCH_CONTACTS, payload: response.data})})
    }
}