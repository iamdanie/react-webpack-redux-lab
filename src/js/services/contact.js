import {BASE_URL} from "./middleware/contacts";
import axios from "axios";

export const register = (contact) => {

    return axios.post(BASE_URL + 'contact/register', contact)
        .catch(error => { console.log("Error: ", error); });
}

export const list = () => {

    return axios.get(BASE_URL + 'contact/list')
        .catch(error => { console.log("Error: ", error); });
}