import axios from 'axios';
import jwt from 'jsonwebtoken';

export const userService = {
	login,
	logout,
	getAll
};

function authHeader() {
	// return authorization header with jwt token
	let user = JSON.parse(localStorage.getItem('user'));

	if (user && user.token) {
			return { 'Authorization': 'Bearer ' + user.token };
	} else {
			return {};
	}
};


function login(email, password) {
	return axios.post('http://localhost:8081/login', { username: email, password: password })
		.then(response => {
			let decoded = jwt.verify(response.data.token.replace('Bearer ', ''), 'fake jwt server 4real');
			
			const user = {
				email: decoded.username,
			};

			localStorage.setItem('user', JSON.stringify(user));
			
			return user;
		});
};

function logout() {
	// remove user from local storage to log user out
	localStorage.removeItem('user');
};

function getAll() {
	const requestOptions = {
			method: 'GET',
			headers: authHeader()
	};

	return fetch('/users', requestOptions).then(handleResponse);
};

function handleResponse(response) {
	if (!response.ok) { 
		return Promise.reject(response.statusText);
	}

	return response.json();
};
