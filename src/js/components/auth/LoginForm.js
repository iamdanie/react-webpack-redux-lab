import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Text } from 'react-form';

class LoginForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: '',
			submitted: false
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSubmitFail = this.handleSubmitFail.bind(this);
	}	

	handleSubmit(submittedValues) {
		event.preventDefault();
	
		const credentials = {
			email: submittedValues.email,
			password: submittedValues.password
		}

		this.props.onLogin(credentials);
	}
	
	handleSubmitFail(errors, formApi, onSubmitError ) {
		console.log('erro')
		console.log(errors);
	}
	
	render() {
		return (
			<div>
				<Form 	
					onSubmit={this.handleSubmit}
					onSubmitFailure = {this.handleSubmitFail}>
					{ formApi => (
						<form
							className='form-signin'
							onSubmit={formApi.submitForm} 
							id="loginForm">
							<img className="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"/>

							<label htmlFor="email" className="sr-only">Email</label>
							<Text field="email" id="email" className="form-control" placeholder="Email address" />
							<label htmlFor="password" className="sr-only">Password</label>
							<Text field="password" id="password"  className="form-control" placeholder="Password"/>
							<button type="submit" className="btn btn-lg btn-primary btn-block">SigIn</button>
						</form>
					)}
				</Form>
			</div>
		)
  }
}

LoginForm.propTypes = {
  onLogin: PropTypes.func.isRequired
}

export default LoginForm;