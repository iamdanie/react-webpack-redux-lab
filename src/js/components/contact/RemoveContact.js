import React, { Component } from "react";
import { connect } from "react-redux";
import { removeContact } from "../../storage/contact/contact";

const toProperties = dispatch => {
  return {
    removeContact: contact => dispatch(removeContact(contact))
  };
};

class RemoveBadge extends Component {
  constructor(props) {
    super(props);

    this.state = {
        id: props.id
    }

    this.handleRemove = this.handleRemove.bind(this);
  }

  handleRemove(event) {
    event.preventDefault();
    this.props.removeContact(this.state.id);
  }

  render() {

    return (
        <a href="#" onClick={this.handleRemove} className="badge badge-danger badge-pill">Remove</a>
    );
  }
}

const RemoveContact = connect(null, toProperties)(RemoveBadge);

export default RemoveContact;