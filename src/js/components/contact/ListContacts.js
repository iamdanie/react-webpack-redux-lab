import React, {Component} from "react";
import { connect } from "react-redux";
import RemoveContact from "./RemoveContact";
import { fetchAllContacts } from "../../storage/contact/contact";
import store from "../../storage/index";
import ReactTable from 'react-table';
import matchSorter from 'match-sorter';
import { Button } from 'semantic-ui-react';

let dataSource = [];
let filterResult = [];

const matchStateToProps = state => {
  const stateContacts = state.contactController.contacts;
  let contacts =  stateContacts.map(contact => {

    return {... contact, ... {key: contact.id}}
  })

  dataSource = contacts;
  filterResult = dataSource;
  
  return { data: stateContacts };
};

const matchDispatchToProps = dispatch => {
  return {
    listAll: () => dispatch(fetchAllContacts())
  };
};

class ContactsTable extends Component {

  constructor(props){
    super(props);

    this.props.listAll();

    this.onCreateClick = this.onCreateClick.bind(this);
  }

  onCreateClick() {
    this.props.history.push({
      pathname: `/create`,
    })
  }

  render() {
    return (
      <div>
        <ReactTable
          data={dataSource}
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value}
          columns={[
            {
              Header: "Name",
              columns: [
                {
                  Header: "First Name",
                  accessor: "firstName",
                  filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["firstName"] }),
                  filterAll: true
                },
                {
                  Header: "Last Name",
                  id: "lastName",
                  accessor: d => d.lastName,
                  filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["lastName"] }),
                  filterAll: true
                }
              ]
            },
            {
              Header: "Info",
              columns: [
                {
                  Header: "Email",
                  accessor: "email",
                  filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["email"] }),
                  filterAll: true
                },
                {
                  Header: "Phone Number",
                  accessor: "phone",
                  filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["phone"] }),
                  filterAll: true
                }
              ]
            }
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <p></p>
        <Button onClick={this.onCreateClick} primary>Create</Button>
      </div>
    );
  }
}

const ListContacts = connect(matchStateToProps, matchDispatchToProps)(ContactsTable);

export default ListContacts;
