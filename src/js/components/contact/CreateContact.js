import React, { Component } from "react";
import { connect } from "react-redux";
import uuidv4 from "uuid";
import { addContact } from "../../storage/contact/contact";
import { Button, Label } from 'semantic-ui-react';
import { Form, StyledText } from 'react-form';

const errorValidation = values => {

  return {
    firstName: !values.firstName ? "You must provide your first name" : null,
    lastName: !values.lastName ? "You must provide your last name" : null,
    email: !values.email || !values.email.match(/\S+@\S+\.\S+/) ? "You must provide a valid email": null,
    phone: !values.phone || !values.phone.match(/^\d+$/) ? "You must provide a valid phone number": null
  };
};

const matchDispatchToProps = dispatch => {
  return {
    addContact: contact => dispatch(addContact(contact))
  };
};

class CreateForm extends Component {
  constructor() {
    super();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitFail = this.handleSubmitFail.bind(this);
    this.hasErrors = this.hasErrors.bind(this);
  }

  handleSubmit(submittedValues) {
    const { firstName, lastName, email, phone } = submittedValues;

    event.preventDefault();
    this.props.addContact({ firstName, lastName, email, phone });
    this.props.history.push({
      pathname: `/`,
    })
  }

  handleSubmitFail(errors, formApi, onSubmitError ) {
    console.log("Errors: ", errors);
  }
  
  hasErrors(e) {

    let hasErrors = false;
    const errors = e.errors;

    for (const key in errors) {
      if (errors.hasOwnProperty(key) && errors[key] !== null) {
          hasErrors = true;
          break;
      }
    }

    return hasErrors;
  }

  render() {
    return (
      <div>
        <Form
          onSubmit={this.handleSubmit}
          onSubmitFailure={this.handleSubmitFail}
          validateError={errorValidation}
        >
          { formApi => (
            <form onSubmit={formApi.submitForm} className="ui form">
            <div className="required field">
              <label htmlFor="firstName">First name</label>
              <StyledText field="firstName" id="firstName"  />
            </div>
            <div className="required field">
              <label htmlFor="lastName">Last name</label>
              <StyledText field="lastName" id="lastName" />
            </div>
            <div className="required field">
              <label htmlFor="email">Email</label>
              <StyledText field="email" id="email"/>
            </div>
            <div className="required field">
              <label htmlFor="phone">Phone</label>
              <StyledText field="phone" id="phone" minLength={8} maxLength={11} />
            </div>
              <Button type='submit' primary disabled={this.hasErrors(formApi)}>Add Contact</Button>
            </form>
          )}
        </Form>
      </div>
    );
  }
}

const CreateContact = connect(null, matchDispatchToProps)(CreateForm);

export default CreateContact;