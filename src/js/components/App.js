import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../storage/index';
import { PrivateRoute } from '../components/navigation/privateRouter';
import HomePage from '../components/pages/home';
import { LogInPage } from '../containers/auth/LogInPage';
import ListContacts from "./contact/ListContacts";
import CreateContact from "./contact/CreateContact";

class App  extends React.Component {
	constructor(props) {
		super(props);

		const { dispatch } = this.props;
		history.listen((location, action) => {
		});
	}

	render() {
		return (
			<div className="jumbotron">
				<div className="container">
					<div className="col-sm-8 col-sm-offset-2">
							<Router history={history}>
								<div>
									<PrivateRoute exact path="/" component={ListContacts} />
									<PrivateRoute exact path="/create" component={CreateContact} />
									<Route path="/login" component={LogInPage} />
								</div>
							</Router>
					</div>
				</div>
			</div>
		);
	}
};

const connectedApp = connect(null)(App);

export { connectedApp as App }; 
