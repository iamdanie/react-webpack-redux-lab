import authOperations from './authOperations';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

const authController = (state = initialState, action) => {
	 return authOperations.hasOwnProperty(action.type) ? authOperations[action.type](state, action) : state;
};

export default authController;
