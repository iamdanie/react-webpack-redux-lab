import logInRequest from './operation/logInRequest';
import logInSuccess from './operation/logInSuccess';
import logInFailure from './operation/logInFailure';
import logOut from './operation/logOut'

import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from '../../storage/auth/actions';

const authOperations =  {
	[LOGIN_REQUEST]: logInRequest,
	[LOGIN_SUCCESS]: logInSuccess,
	[LOGIN_FAILURE]: logInFailure,
	[LOGOUT]: logOut
}

export default authOperations;
