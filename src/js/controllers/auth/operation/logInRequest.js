const logInRequest = (state, action) => {
	return {
		...state,
		loggedIn: true,
		user: action.user
	}
};

export default logInRequest;
