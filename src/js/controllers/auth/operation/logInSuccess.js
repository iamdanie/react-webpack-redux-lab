const logInSuccess = (state, action) => {
	return {
		...state,
		loggingIn: true,
    user: action.user
	}
};

export default logInSuccess;
