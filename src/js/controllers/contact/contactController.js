import contactOperations from "./contactOperations"

const initialState = {
    contacts: []
};
  
const contactController = (state = initialState, action) => {

    return contactOperations.hasOwnProperty(action.type) ? contactOperations[action.type](state, action) : state;
};

export default contactController;
