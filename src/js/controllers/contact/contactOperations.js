import { ADD_CONTACT, FETCH_CONTACTS, REMOVE_CONTACT } from "../../storage/contact/actions";
import addOne from "./operations/addOne";
import removeOne from "./operations/removeOne";
import listAll from "./operations/listAll";

const contactOperations = {
    [ADD_CONTACT]: addOne,
    [REMOVE_CONTACT]: removeOne,
    [FETCH_CONTACTS]: listAll
}

export default contactOperations;