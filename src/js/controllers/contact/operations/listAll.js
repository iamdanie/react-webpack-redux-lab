const listAll = (state, action) => {
    return { ...state, contacts: action.payload };
};

export default listAll;
