const addOne = (state, action) => {

    return { ...state, contacts: [...state.contacts, action.payload] };
};

export default addOne;
