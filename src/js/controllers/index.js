import authController from './auth/authController';
import contactController from './contact/contactController';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
	authController,
	contactController
});

export default rootReducer;
