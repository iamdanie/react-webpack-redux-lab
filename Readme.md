# Introduction

React Lab application integrating Redux + React-Semantic UI + react-form

## Installation

Run 

```bash
yarn
```

For our demo API, you will need to clone and install:

https://gitlab.com/iamdanie/nodejs-backend-lab

And for our login, you will need to clone and install this fake JWT server:

https://github.com/emersonscytech/fake-jwt-server

(Please, follow the instructions inside each repository's Readme)

## Run the application

```bash
yarn start
```

Your web dev server will be running on http://localhost:8080

Cheers ;)
